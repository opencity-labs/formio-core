import {nodeResolve} from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import typescript from "@rollup/plugin-typescript";
import dts from "rollup-plugin-dts";
import terser from "@rollup/plugin-terser";
import peerDepsExternal from "rollup-plugin-peer-deps-external";
import postcss from "rollup-plugin-postcss";
import { babel } from '@rollup/plugin-babel';
import autoprefixer from 'autoprefixer';
import copy from "rollup-plugin-copy";
import nodeBuiltins from 'rollup-plugin-node-builtins';
import nodeGlobals from 'rollup-plugin-node-globals';
import externalGlobals from "rollup-plugin-external-globals";
import json from '@rollup/plugin-json';

const prefixer = require('postcss-prefix-selector')


const packageJson = require("./package.json");

export default [
    {
        input: "src/index.ts",
        output: [
            {
                file: packageJson.main,
                format: "cjs",
                sourcemap: true,
            },
            {
                file: packageJson.module,
                format: "esm",
                sourcemap: true,
            },
            {
                file: 'dist/umd/index.js',
                format: 'umd',
                sourcemap: true,
                name: "formioCore",
                esModule: false,
            },
            {
                format: 'iife',
                sourcemap: true,
                name: 'formioCore',
                file: 'dist/iife/index.js',
            }
        ],
        plugins: [
            peerDepsExternal(),
            babel({
                babelHelpers: 'bundled',
                extensions: ['.js', '.jsx', '.ts', '.tsx'],
                exclude: ['node_modules/**','./src/stories/**'],
                plugins: [
                    [
                        'module-resolver',
                        {
                            root: ['src'],
                        },
                    ],
                ],
                presets: ['@babel/preset-react'],
            }),
            nodeResolve({ jsnext: true }),
            nodeGlobals(),
            nodeBuiltins(),
            commonjs({
                exclude: ['node_modules/@formio/**','./src/stories'],
                namedExports: {
                    'node_modules/lodash/index.js': ['get','padStart'],
                }
            }),
            typescript({tsconfig: "./tsconfig.json",exclude: "./src/stories"}),
            json(),
            (process.env.NODE_ENV === 'production' && terser()),
            externalGlobals(['lodash-es']),
           postcss({
                plugins: [
                    autoprefixer(),
                    prefixer({
                        prefix: '.ocl',
                        // Optional transform callback for case-by-case overrides
                        transform: function (prefix, selector, prefixedSelector, filePath, rule) {
                            console.log(selector)
                            if (selector === '.gu-mirror' || selector === '.gu-transit'
                                || selector === '.gu-unselectable' || selector === '.gu-hide'
                                || selector.startsWith('.modal') || selector === '.fade' || selector === 'body'
                            ) {
                                return selector;
                            } else {
                                return prefixedSelector;
                            }
                        }
                    })],
                inject: false,
                sourceMap: (process.env.NODE_ENV === 'production' ? false : 'inline'),
                minimize: (process.env.NODE_ENV === 'production'),
                extract: 'ocl.min.css'
            }),

           copy({
                targets: [
                    { src: 'src/assets/styles/fonts', dest: 'dist/' },
                    { src: 'dist/cjs/ocl.min.css', dest: 'dist/' }
                ],
               force: true,
               verbose: true,
               hook: 'closeBundle'
            })
        ],
        external: ["react","lodash.js","react-countdown","design-react-kit","axios","react-dom","formiojs","@opencitylabs/bootstrap-italia/bootstrapItalia","@r2wc/react-to-web-component","@formio/js","@formio/react","react/jsx-runtime","react-dom/client",/\.css$/u],
    },
    {
        external: [/\.css$/],
        input: "dist/esm/types/index.d.ts",
        output: [{ file: "dist/index.d.ts", format: "esm" }],
        plugins: [dts.default()],
    },
];
