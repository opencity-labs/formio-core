import React from 'react';
import ReactDOM from 'react-dom/client';
import Demo from "./pages/Demo";
import './assets/styles/App.scss';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
    <Demo></Demo>
  </React.StrictMode>
);
