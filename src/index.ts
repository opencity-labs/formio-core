import r2wc from '@r2wc/react-to-web-component';
import {AppForm, BuildForm} from './components';

const WebAppForm = r2wc(AppForm,{
    props: {
        url: 'string',
        form: 'string',
        readOnly: 'boolean',
        submission: 'json',
        onChange: 'function',
        formReady: 'function',
        options: 'json'
    }
});

const WebBuildForm = r2wc(BuildForm,{
    props: {
        url: 'string',
        form: 'string',
        submission: 'json',
        onChange: 'function',
        options: 'json'
    }
});

customElements.define('render-form', WebAppForm);
customElements.define('build-form', WebBuildForm);
export {WebAppForm};
export {WebBuildForm};


