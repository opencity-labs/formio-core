"use strict";
exports.__esModule = true;
var react_to_web_component_1 = require("@r2wc/react-to-web-component");
var components_1 = require("./components");
var WebAppForm = react_to_web_component_1["default"](components_1.AppForm, {
    props: {
        url: 'string',
        form: 'string',
        readOnly: 'boolean',
        submission: 'json',
        onChange: 'function',
        formReady: 'function',
        options: 'json'
    }
});
exports.WebAppForm = WebAppForm;
var WebBuildForm = react_to_web_component_1["default"](components_1.BuildForm, {
    props: {
        url: 'string',
        form: 'string',
        readOnly: 'boolean',
        submission: 'json',
        onChange: 'function',
        formReady: 'function',
        options: 'json'
    }
});
exports.WebBuildForm = WebBuildForm;
customElements.define('render-form', WebAppForm);
customElements.define('build-form', WebBuildForm);
