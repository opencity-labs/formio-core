import editForm from './DynamicCalendar.form';
import React, {useEffect, useRef, useState} from 'react';
import {ReactComponent} from '@formio/react';
import {createRoot} from 'react-dom/client';
import { DayPicker } from "react-day-picker";
import Countdown from 'react-countdown';
import "react-day-picker/dist/style.css";
import { it,de,enGB } from "date-fns/locale";
import moment from "moment";
import 'moment/locale/it';
import 'moment/locale/en-gb';
import 'moment/locale/de';
import {Alert, Icon, Input} from 'design-react-kit';
import {convertIntegerToTime, convertTimeInInteger} from '../../../utils/ConvertTimeInInteger';
import Swal from 'sweetalert2'
import {baseUrl} from '../../../utils/BaseUrl';

 export const ReactDynamicCalendar = ({component,current,onChange,i18next}) => {

     //Data sul calendario selezionata
     const [selected, setSelected] = useState();

     //Date sul calendario disabilitate
     const [disabledDates, setDisabledDates] = useState([]);

     const [daysSlot, setDaysSlot] = useState([]);
     // Mostra errore se non ci sono date disponibili
     const [noAvailableSlot, setNoAvailableSlot] = useState(false);

     //Meeting id restituito dalla chiamata draft
     const [draftMeeting, setDraftMeeting] = useState(null);

     // Slot selezionato
     const [slotSelected, setSlotSelected] = useState(null);

     const [selectedData, setSelectedData] = useState(null);
     const [activeSlot, setActiveSlot] = useState(null);
     //Mostra alert errori
     const [showError, setShowError] = useState(false);

     //Prima data disponibile del mese
     const [ firstAvailableSlot, setFirstAvailableSlot ] = useState({});
     // Dato caledario sola lettura
     const [ renderData, setRenderData ] = useState(null);

     const refButtonSlot = useRef()

     const calendarID = component.calendarId

     moment.locale(i18next.language)


     const calculateAvailableDates = (date) =>{
         let formattedDate = moment(date).format("YYYY-MM-DD")
         fetch(getMonthlyAvailabilitiesUrl(formattedDate))
             .then(response => response.json())
             .then(dateAvailables => {

                 let daysInMonth = getListDaysInMonth(date ? date : new Date())

                 //Filtro solo i giorni disponibili
                 const elements = dateAvailables.filter( (item) => item.available == true);

                 //Conto quandi gionri disponibili ci sono
                 const countAllElmAvailable = elements.length;
                 if(countAllElmAvailable === 0){
                     resetValue()

                 }

                     let calculateAvailableDates = []
                     daysInMonth.forEach(day => {
                         if(dateAvailables.some(item => item.date === day && item.available == true)){
                             calculateAvailableDates.push({available: true, date: day})
                         }else{
                             calculateAvailableDates.push({available: false, date: day})
                         }
                     })
                     const dates =  calculateAvailableDates.map(el =>{
                         if(el.available === false){
                             return moment(el.date, 'YYYY-MM-DD').toDate();
                         }
                     })
                     setDisabledDates(dates)
                     // Seleziono il primo giorno disponibile
                     const firstDayAvailable = elements.length ? elements[0].date : date

                     getDaysSlot(firstDayAvailable)


             })
             .catch(error => console.error(error));
     }

     const getDaysSlot = (date)  => {
         setActiveSlot(null);
         setSelectedData(null);
         setDaysSlot([])
         let formattedDate = moment(date).format("YYYY-MM-DD")

         fetch(getDailyAvailabilitiesUrl(formattedDate))
             .then(response => response.json())
             .then(daysSlot => {
                 const elements = daysSlot.filter( (item) => item.availability == true);
                 setFirstAvailableSlot(elements[0])
                    if(elements.length > 0 ){
                        setDaysSlot(daysSlot)
                        setNoAvailableSlot(false)
                        //Seleziono la data testuale sulla colonna degli slots
                        setSelectedData(moment(date).format("L"))
                        //Seleziono la data sul calendario
                        setSelected(moment(date, 'YYYY-MM-DD').toDate())

                    }else{
                        //Seleziono la data sul calendario
                        if(daysSlot.length > 0){
                            setSelected(moment(daysSlot[0].date, 'YYYY-MM-DD').toDate())
                        }
                        setDaysSlot([])
                        resetValue()
                    }

             })
     }

     const saveRenderData = () =>{
         if (calendarID !== "" && calendarID != null){
             if(!current.dataValue) {
                 calculateAvailableDates(new Date())
             } else {
                 let explodedValue = current.dataValue
                     .replace(")", "")
                     .replace(" (", " @ ")
                     .replace(/\//g, "-")
                     .split(" @ ");
                 let explodedCalendar = explodedValue[2].split("#");

                 const renderData = {
                     date: explodedValue[0],
                     slot: explodedValue[1]
                 }
                 setRenderData(renderData)
             }
         }
     }


     useEffect(() => {
         window?.instanceFormio?.formReady.then( () => {
             setTimeout(() => {
                 saveRenderData()
             },300)

     })

     },[]);

     const getListDaysInMonth = (date) =>{
             let monthDate = moment(date, 'YYYY-MM');
             let daysInMonth = monthDate.daysInMonth();
             let arrDays = [];

             while(daysInMonth) {
                 let current = moment(date).date(daysInMonth);
                 arrDays.push(current.format('YYYY-MM-DD'));
                 daysInMonth--;
             }
             return arrDays;
     }

     const getMonthlyAvailabilitiesUrl = (date, available=false) => {
         let calendarID = component.calendarId,
             selectOpeningHours = component.select_opening_hours,
             openingHours = component.select_opening_hours
                 ? component.opening_hours
                 : [],
             location = window.location,
             explodedPath = location.pathname.split("/");


         let url = `${baseUrl()}/api/calendars/${calendarID}/availabilities`;

         let queryParameters = [
             `from_time=${moment(date).startOf('month').format("YYYY-MM-DD")}`,
             `to_time=${moment(date).endOf("month").format("YYYY-MM-DD")}`,
         ];
         if (available) {
             queryParameters.push(`available=${available}`);
         }
         // filter availabilities by selected opening-hours (this is mandatory in case of overlapped opening hours)
         if (selectOpeningHours && openingHours) {
             // Select specific opening hours
             queryParameters.push(`opening_hours=${openingHours.join()}`);
         }

         return `${url}?${queryParameters.join("&")}`;
     }


     const getDailyAvailabilitiesUrl = (date, available= false) => {
         let calendarID = component.calendarId,
             selectOpeningHours = component.select_opening_hours,
             openingHours = component.select_opening_hours
                 ? component.opening_hours
                 : [],
             location = window.location,
             explodedPath = location.pathname.split("/");

         const formattedDate = moment(date).format("YYYY-MM-DD");
         let url = `${baseUrl()}/api/calendars/${calendarID}/availabilities/${formattedDate}`;

         let queryParameters = [];
         if (available) {
             queryParameters.push(`available=${available}`);
         }
         if (self.meeting) {
             // Exclude saved meeting from unavailabilities
             queryParameters.push(`exclude=${self.meeting}`);
         }
         if (selectOpeningHours && openingHours) {
             // Select specific opening hours
             queryParameters.push(`opening_hours=${openingHours.join()}`);
         }

         if (queryParameters) {
             url = `${url}?${queryParameters.join("&")}`;
         }

         return url;
     }

     const onMonthChange = (data) =>{
         calculateAvailableDates(data)
     }

     const onDayClick = (data) => {
         getDaysSlot(data)
     }

     const onClickSelectDate = (element) => {
         setActiveSlot(element.target.id);
         if(element){
             setSlotSelected(element.target.dataset)
         }
     };

     const createOrUpdateMeeting = (valueSlot) => {

         setDraftMeeting(null)

          let url = `${baseUrl()}/it/meetings/new-draft`,
             dataMeet =  {
                 "date": slotSelected?.date ? moment(slotSelected?.date).format('DD-MM-YYYY') : moment(valueSlot.target.dataset.date).format('DD-MM-YYYY'),
                 "slot": slotSelected?.value_start && slotSelected?.value_end ? slotSelected.value_start+'-'+slotSelected.value_end : valueSlot.target.dataset.slots,
                 "calendar": component.calendarId,
                 "opening_hour": slotSelected?.opening_hour ? slotSelected?.opening_hour : valueSlot.target.dataset.opening_hour,
                 "meeting": draftMeeting ? draftMeeting.id: '',
                 "first_available_date": firstAvailableSlot.date,
                 "first_available_start_time": firstAvailableSlot.start_time,
                 "first_available_end_time": firstAvailableSlot.end_time,
                 "first_availability_updated_at": moment().format(),
             };

         fetch(url,{
             method: "POST",
             body: JSON.stringify(dataMeet),
             headers:{
                 'Content-Type': 'application/json; charset=utf-8',
             },
             strictErrors: true
         })
             .then(response => {
                 if (!response.ok) {
                     throw new Error(i18next.t('Intervallo selezionato non disponibile!'));
                 }
                 return response.json();
             })
             .then(draftMeet => {
                 setDraftMeeting(draftMeet)
             }).catch(e => {
                 console.log(e)
             Swal.fire({
                 title: '',
                 html: "<p>"+e+"</p>",
                 icon: 'error',
                 cancelButtonText: i18next.t('Chiudi'),
                 showCancelButton: true,
                 showConfirmButton: false
             })

         })
     }

     useEffect(() => {
         if(draftMeeting && slotSelected){
             setValueCalendar()
         }
     }, [draftMeeting,slotSelected]);


     useEffect(() => {
         if(slotSelected){
             //Creo l'appuntamento in bozza
             createOrUpdateMeeting()
         }
     },[slotSelected])


     const Completionist = () => <span>{i18next.t('Tempo prenotazione scaduto!')}</span>;

     // Renderer CountDown callback with condition
     const renderer = ({  minutes, seconds, completed }) => {
         if (completed) {
             // Render a completed state
             return <Completionist/>;
         } else {
             // Render a countdown
             return <div><b>{i18next.t('Giorno selezionato per la prenotazione')}: </b> {selectedData} {slotSelected ? `${i18next.t('alle')} ${slotSelected.value_start}-${slotSelected.value_end}` : ''}
                 <div> <span> <i>{i18next.t('Per confermare la prenotazione che ti abbiamo riservato procedi con l’invio di questa pratica e con il pagamento (se richiesto) di quanto previsto entro')}:</i>
                     {' '}<b>{minutes} {i18next.t('minuti')} {seconds} {i18next.t('secondi')}</b></span></div>
             </div>;
         }
     };


     const handleFromTime = (e) =>{
         const currentValue = convertTimeInInteger(e.target.value)
         const valueMin = convertTimeInInteger(e.target.min)
         const valueMax = convertTimeInInteger(e.target.max)
        if((currentValue < valueMin || currentValue > valueMax) || currentValue > convertTimeInInteger(slotSelected.end)){
            Swal.fire({
                title: '',
                html: `<p>${i18next.t("l'orario")} <b>${convertIntegerToTime(currentValue)}</b> ${i18next.t('non è valido')}</p>`,
                icon: 'error',
                cancelButtonText: i18next.t('Chiudi'),
                showCancelButton: true,
                showConfirmButton: false
            })
            setValueCalendar('')
        } else {
            setSlotSelected({...slotSelected,value_start: e.target.value})
        }
     }

     const handleToTime = (e) =>{
         const currentValue = convertTimeInInteger(e.target.value)
         const valueMin = convertTimeInInteger(e.target.min)
         const valueMax = convertTimeInInteger(e.target.max)

         if((currentValue < valueMin || currentValue > valueMax) || currentValue < convertTimeInInteger(slotSelected.start)){
             Swal.fire({
                 title: '',
                 html: `<p>${i18next.t("l'orario")} <b>${convertIntegerToTime(currentValue)}</b> ${i18next.t('non è valido')}</p>`,
                 icon: 'error',
                 cancelButtonText: i18next.t('Chiudi'),
                 showCancelButton: true,
                 showConfirmButton: false
             })
             setValueCalendar('')
         }else{
             setSlotSelected({...slotSelected,value_end: e.target.value})
         }
     }


     // Imposto il valore del calendario per formio
     const setValueCalendar = (value) =>{
         if(value === null || value === ''){
             //Faccio reset del valore di formio
             onChange('')
         }else{
             onChange(`${selectedData.replace(/-/g, "/")} @ ${slotSelected.value_start}-${slotSelected.value_end} (${calendarID}#${draftMeeting.id}#${slotSelected.opening_hour})`)
         }
     }

     //Reset dei valori di prenotazione
     const resetValue = () =>{
         setNoAvailableSlot(true)
         setDraftMeeting(null)
         setSelectedData(null)
         setValueCalendar(null)
     }

     // Recupero il locale per il comp. DayPicker
     const getCurrentLocale = () =>{
         if(i18next.language === 'it'){
             return it
         }else if (i18next.language === 'en')
             return enGB
         else if (i18next.language === 'de')
             return de
     }

     return (
         <>
             {renderData ?
                 <div className="my-5 d-print-block d-preview-calendar d-preview-calendar-none">
                     <span>{i18next.t('Giorno selezionato per la prenotazione')}: </span> <b>{renderData.date}</b> {i18next.t('alle')} {i18next.t('ore')} <b>{renderData.slot}</b></div>
                 :
                 <div className="slot-calendar d-print-none d-preview-calendar-none">
                     <div className="row">
                         <div className="col-12 col-md-6">
                             <div className={'my-3'}>
                                 <h6 className={'mx-3 px-1'}>{i18next.t(component.label)}</h6>
                                 <DayPicker mode="single" selected={selected} locale={getCurrentLocale()} fromDate={new Date()}
                                            disabled={disabledDates} onMonthChange={onMonthChange}
                                            onDayClick={onDayClick}/>
                             </div>
                         </div>
                         <div className="col-12 col-md-6">
                             <div className="mt-3">
                                 <div className={'col-12'}>{selectedData ?
                                     <h6>{i18next.t('Orari disponibili del')} {selectedData}</h6> : null}</div>
                                 {daysSlot.map((slot, index) => (
                                     <div className="col-6" key={'slot-' + index}>
                                         <button type="button" id={'slot-' + index} onClick={onClickSelectDate} ref={refButtonSlot}
                                                 className={activeSlot === 'slot-' + index ? 'active btn btn-slot p-0' : 'btn btn-slot p-0'}
                                                 tabIndex="1" data-date={slot.date}
                                                 data-start={slot.start_time}
                                                 data-end={slot.end_time}
                                                 data-value_start={slot.start_time}
                                                 data-value_end={slot.end_time}
                                                 data-slots={slot.start_time + '-' + slot.end_time}
                                                 data-opening_hour={slot.opening_hour}
                                                 data-min_duration={slot.min_duration}
                                                 disabled={slot.slots_available == 0}
                                         >
                                             {slot.start_time} - {slot.end_time}
                                         </button>
                                     </div>))}
                                 {noAvailableSlot ?
                                     <div className="callout callout-highlight warning">
                                         <div className="callout-title">
                                             <Icon
                                                 aria-hidden
                                                 icon="it-help-circle"
                                             />
                                             {i18next.t('Attenzione')}
                                         </div>
                                         <p> {i18next.t('Non è possibile prenotare per la data selezionata')}.</p>
                                     </div>
                                     : null}
                             </div>
                         </div>
                         <div className="col-12">
                             <div
                                  className="mt-3 d-print-block d-preview-calendar px-4 d-none">{selectedData ?
                                 <div><b>{i18next.t('Giorno selezionato per la prenotazione')}: </b> {selectedData} {slotSelected ? `${i18next.t('alle')} ${slotSelected.value_start}-${slotSelected.value_end}` : ''}
                                 </div> : null}</div>


                             {draftMeeting ? <div className={'row'}>
                                     <div className={'col-12 mb-3'}><p className={'mb-3'}><b>{i18next.t('Se preferisci puoi utilizzare il controllo che segue per accorciare la durata della prenotazione')}</b>
                                     </p></div>
                                     <div className={'col-12 col-md-6 mb-4'}>
                                         <Input
                                             type="time"
                                             label={i18next.t('Dalle')}
                                             className="active"
                                             defaultValue={slotSelected.value_start || slotSelected.start}
                                             min={slotSelected.start}
                                             max={slotSelected.end}
                                             onBlur={handleFromTime}
                                         />
                                     </div>
                                     <div className={'col-12 col-md-6 mb-4'}>
                                         <Input
                                             type="time"
                                             label={i18next.t('alle')}
                                             className="active"
                                             defaultValue={slotSelected.value_end || slotSelected.end}
                                             onBlur={handleToTime}
                                             min={slotSelected.start}
                                             max={slotSelected.end}
                                         />
                                     </div>
                                 <div className={'col-12 mb-4'}>
                                     <Alert color={'info'} defaultValue={'string'}>
                                         <Countdown date={draftMeeting.expiration_time} renderer={renderer}></Countdown>
                                     </Alert>
                                 </div>
                                 </div>
                                 : null}

                         </div>
                     </div>
                 </div>}
         </>

     )

 };


export default class FormioDynamicCalendar extends ReactComponent {

    /**
     * This function tells the form builder about your component. It's name, icon and what group it should be in.
     *
     * @returns {{title: string, icon: string, group: string, documentation: string, weight: number, schema: *}}
     */
    static get builderInfo() {
        return {
            title: 'Dynamic Calendar',
            group: 'basic',
            icon: 'bi bi-calendar-plus',
            documentation: '',
            weight: 70,
            schema: FormioDynamicCalendar.schema()
        };
    }

    /**
     * This function is the default settings for the component. At a minimum you want to set the type to the registered
     * type of your component (i.e. when you call Components.setComponent('type', MyComponent) these types should match.
     *
     * @param sources
     * @returns {*}
     */
    static schema() {
        return ReactComponent.schema({
            type: 'dynamic_calendar'
        });
    }


    /*
     * Defines the settingsForm when editing a component in the builder.
     */
    static editForm = editForm;

    /**
     * This function is called when the DIV has been rendered and added to the DOM. You can now instantiate the react component.
     *
     * @param DOMElement
     * #returns ReactInstance
     */
    attachReact(element, ref) {
            this.rootComponent = createRoot(element);
            this.rootComponent.render(
                <ReactDynamicCalendar
                    ref={ref}
                    component={this.component} // These are the component settings if you want to use them to render the component.
                    value={this.dataValue} // The starting value of the component.
                    onChange={e => this.setValue(e)} // The onChange event to call when the value changes.
                    {...this}
                    current={this}
                />);
    }


    setValue(value) {
        if (value === undefined || value === null) {
            return;
        }
        if (value === ''){
            this.updateValue('')
            return
        }

        this.updateValue(value)
        this.shouldSetValue = true

        let explodedValue = value
            .replace(")", "")
            .replace(" (", " @ ")
            .replace(/\//g, "-")
            .split(" @ ");
        let explodedCalendar = explodedValue[2].split("#");

        this.date = explodedValue[0];
        this.slot = explodedValue[1];
        this.meeting = explodedCalendar[1];
        this.meeting_expiration_time = null;
        this.opening_hour =
            explodedCalendar.length === 3 ? explodedCalendar[2] : "";
    }

    getValue() {
        if (!(this.date && this.slot && this.meeting && this.opening_hour)) {
            // Unset value (needed for calendars with 'required' option"
            return null;
        }
        let meeting_id = this.meeting ? this.meeting : "";
        let opening_hour = this.opening_hour ? this.opening_hour : "";

        return `${this.date.replace(/-/g, "/")} @ ${this.slot} (${
            this.component.calendarId
        }#${meeting_id}#${opening_hour})`;
       // return super.getValue();
    }

    constructor(component, options, data) {
        super(component, options, data);
    }

    /**
     * Called immediately after the component has been instantiated to initialize
     * the component.
     */
    init() {
        super.init();
    }


    /**
     * The second phase of component building where the component is rendered as an HTML string.
     *
     * @returns {string} - The return is the full string of the component
     */
    render() {
        // For react components, we simply render as a div which will become the react instance.
        // By calling super.render(string) it will wrap the component with the needed wrappers to make it a full component.
        return super.render(`<div ref="react-${this.id}"></div>`);
    }

}