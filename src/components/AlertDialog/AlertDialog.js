import React from "react";
import {Button, Icon, Modal, ModalBody, ModalFooter} from 'design-react-kit';
import {useEffect, useState} from 'react';

export const AlertDialog = ({error,onClosed}) => {
    const [isOpen, toggleModal] = useState(false);

    useEffect(() => {
        if(error){
            toggleModal(!isOpen)
        }
    }, [error]);

    const handleClosed = () => {
        onClosed(false)
    }
    return (
        <div>
            <Modal
                isOpen={isOpen}
                toggle={() => toggleModal(!isOpen)}
                onClosed={handleClosed}
                centered
            >
                <ModalBody>
                    <Icon className={'d-flex justify-content-center m-auto'} icon={'it-error'} size={'xl'} color={'danger'}></Icon>
                    <p className={'text-center mt-3'}>l'orario  <b>{error ? error : null}</b> non è valido</p>
                </ModalBody>
                <ModalFooter>
                    <Button color='secondary' onClick={() => toggleModal(!isOpen)}>
                        Chiudi
                    </Button>
                </ModalFooter>
            </Modal>
        </div>
    )
}