export interface BuildFormProps {
    submission?: object;
    form?: any;
    url?: string;
    onChange?: Function;
    onEditComponent?: Function;
    onCancelComponent?: Function;
    onDeleteComponent?: Function;
    onUpdateComponent?: Function;
    options?:object;
}
