export interface RenderFormProps {
    url: string;
    submission?: object;
    form?: object;
    formReady?: Function;
    readOnly?: boolean;
    onChange?: Function;
    options?:object;
}
