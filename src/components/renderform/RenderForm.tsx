import React, {useEffect} from "react";
import '../../assets/styles/App.scss';
import {RenderFormProps} from "./RenderForm.types";
import {Components, Form, Templates, Formio} from "@formio/react";
import FormioPageBreak from "../customcomponent/PageBreak/PageBreak";
import SdcFile from "../customcomponent/FileSdc/SdcFile";
import bootstrapItalia from '@opencitylabs/bootstrap-italia/bootstrapItalia';
import Toggle from "../customcomponent/Toggle";
import FormioHelper from "../../utils/FormioHelper";
import FormioDynamicCalendar from "../customcomponent/DynamicCalendar/DynamicCalendar";
import {DefaultFormioTranslations} from "../../locales/defaultFormioTranslations";
import '../../locales/i18n'
import {merge} from "lodash-es";
import { useTranslation } from 'react-i18next';


Components.addComponent('pagebreak', FormioPageBreak)
Components.addComponent('dynamic_calendar', FormioDynamicCalendar)
//Components.addComponent('sdcfile',SdcFile)
Components.addComponent('toggle', Toggle)

Formio.use(bootstrapItalia);

Templates.framework = bootstrapItalia
// @ts-ignore
window.FormioHelper = new FormioHelper()

const RenderForm: React.FC<RenderFormProps> = ({url, readOnly, submission, form, options}) => {

    const [translations, setTranslations] = React.useState({});
    const [loading, setLoading] = React.useState(true);
    const {i18n} = useTranslation()
    const [optionsFormio, setOptionsFormio] = React.useState({
        readOnly: readOnly,
        noAlerts: false,
        hide: true,
        buttonSettings: {
            showPrevious: false,
            showNext: false,
            showSubmit: false,
            showCancel: false
        },
        language: i18n.language,
        i18n: DefaultFormioTranslations()
    })

    useEffect(() => {
        if (url && loading) {
            fetch(`${url}/i18n?lang=${i18n.language}`)
                .then(r => (r.json()))
                .then(json => {
                setTranslations(json)
            })
        }
    }, []);

    useEffect(() => {
        if(Object.keys(translations).length > 0){
            setOptionsFormio({
                ...optionsFormio,
                i18n: merge({}, DefaultFormioTranslations(), translations)
            })
        }

    }, [translations]);

    useEffect(() => {
        if(Object.keys(translations).length > 0) {
            setLoading(false)
        }
    }, [optionsFormio]);

    const instanceFormio = (form: any) => {
        // @ts-ignore
        window.translationsFormio = merge({}, DefaultFormioTranslations(), translations)

        // @ts-ignore
        window.instanceFormio = form
    }

    const onChange = (form: any) => {
        console.log('change',form);
    }


    return (
        <div className={'ocl'}>
            {!loading ? (<Form src={url} form={form} options={options ? options : optionsFormio} submission={submission}
                               formReady={instanceFormio} onChange={onChange}/>) : null}
        </div>
    )
        ;
}

export default RenderForm;
