class BasePath {

    getBasePath(){
        const explodedPath = window.location.pathname.split("/");
        return 'https://servizi.comune-qa.bugliano.pi.it/lang' || location.origin + '/' + explodedPath[1];
    }

}

export default BasePath;