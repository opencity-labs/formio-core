import React from 'react';
import './page.css';
import BuildForm from "../components/buildform";
import {BuildFormProps} from "../components/buildform/BuildForm.types";
import {Components} from "@formio/react";
import FormioPageBreak from "../components/customcomponent/PageBreak/PageBreak";
Components.addComponent('pagebreak',FormioPageBreak)

export const PageBuilder: React.FC<BuildFormProps> = ({url, form,onChange, options}) => {

  return (
      <section className="storybook-page-build">
        <BuildForm form={form} options={options} url={url}></BuildForm>
      </section>
)
  ;
};
