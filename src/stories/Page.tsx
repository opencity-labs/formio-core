import React from 'react';
import './page.css';
import RenderForm from "../components/renderform";
import {RenderFormProps} from "../components/renderform/RenderForm.types";


export const PageFormRender: React.FC<RenderFormProps> = ({url,readOnly,submission,form,onChange,formReady, options}) => {

  return (
      <section className="storybook-page">
        <RenderForm url={url} readOnly={readOnly} submission={submission} options={options}></RenderForm>
      </section>
)
  ;
};
