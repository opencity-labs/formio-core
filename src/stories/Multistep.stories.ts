import type { Meta, StoryObj } from '@storybook/react';
import {PageFormRender} from "./Page";


const meta = {
  title: 'Lista componenti/Servizio con step multipli',
  component: PageFormRender,
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ['autodocs'],
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/configure/story-layout
    layout: 'fullscreen',
  },
  args: {
  },
} satisfies Meta<typeof PageFormRender>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Render: Story = {
  args: {
    url: 'https://form-qa.stanzadelcittadino.it/form/65240f0d6e174700542ff4d7',
    submission:{"data":{"sdcfile":[],"test": "testo","applicant":{"data":{"email_address":"test@test.it","completename":{"data":{"name":"Marco","surname":"Polo"}},"gender":{"data":{"gender":"maschio"}},"fiscal_code":{"data":{"fiscal_code":"PLOMRC01P30L736Y"}},"Born":{"data":{"natoAIl":"01\/09\/1976","place_of_birth":"Ponsacco"}},"address":{"data":{"address":"Via Gramsci, 1","house_number":"","municipality":"Bugliano","county":"TN","postal_code":"56056"}}}}}}
  },
};

