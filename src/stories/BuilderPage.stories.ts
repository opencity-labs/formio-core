import type { Meta, StoryObj } from '@storybook/react';
import {PageBuilder} from "./BuilderForm";


const meta = {
  title: 'Lista componenti/Builder',
  component: PageBuilder,
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ['autodocs'],
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/configure/story-layout
    layout: 'fullscreen',
  },
  args: {
  },
} satisfies Meta<typeof PageBuilder>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Builder: Story = {
  args: {
    url: 'https://form-qa.stanzadelcittadino.it/form/6345270e7df272004dd69349'
  },
};

