import type { Meta, StoryObj } from '@storybook/react';
import {PageFormRender} from "./Page";


const meta = {
  title: 'Lista componenti/Calendario Dinamico',
  component: PageFormRender,
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ['autodocs'],
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/configure/story-layout
    layout: 'fullscreen',
  },
  args: {
  },
} satisfies Meta<typeof PageFormRender>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Render: Story = {
  args: {
    url: 'https://form-qa.stanzadelcittadino.it/form/6659f0c643367e562186eb17',
    submission:{"data":{"sdcfile":[],"test": "testo","applicant":{"data":{"calendar": "26/07/2024 @ 11:30-12:00 (9bf8c135-2ff5-4ffa-be1a-bd794a3171fd#03b2a7bc-17dd-49d9-904c-759bf1005d52#cbb68f7a-de36-4693-be65-dc9b93f283fd)","email_address":"test@test.it","completename":{"data":{"name":"Marco","surname":"Polo"}},"gender":{"data":{"gender":"maschio"}},"fiscal_code":{"data":{"fiscal_code":"PLOMRC01P30L736Y"}},"Born":{"data":{"natoAIl":"01\/09\/1976","place_of_birth":"Ponsacco"}},"address":{"data":{"address":"Via Gramsci, 1","house_number":"","municipality":"Bugliano","county":"TN","postal_code":"56056"}}}}}}

  },
};

