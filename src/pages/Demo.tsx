import React from "react";
import RenderForm from "../components/renderform";
import "../assets/styles/App.scss"
import 'react-tabs/style/react-tabs.css';
import {Tab, TabList, TabPanel, Tabs} from "react-tabs";
import BuildForm from "../components/buildform";

function Demo() {
    return (
        <Tabs>
            <TabList>
                <Tab>Form Render</Tab>
                <Tab>Builder Form</Tab>
            </TabList>

            <TabPanel>
                <RenderForm url={'https://form-qa.stanzadelcittadino.it/form/6659f0c643367e562186eb17'}
                            readOnly={false}
                            submission={{'data': { "calendar": "26/07/2024 @ 11:30-12:00 (9bf8c135-2ff5-4ffa-be1a-bd794a3171fd#03b2a7bc-17dd-49d9-904c-759bf1005d52#cbb68f7a-de36-4693-be65-dc9b93f283fd)"}}}
                >

                </RenderForm>
            </TabPanel>
            <TabPanel>
                <BuildForm url={'https://form-qa.stanzadelcittadino.it/form/65240f0d6e174700542ff4d7'} form={'https://form-qa.stanzadelcittadino.it/form/65240f0d6e174700542ff4d7'}></BuildForm>
            </TabPanel>
        </Tabs>

    )
}

export default Demo;
