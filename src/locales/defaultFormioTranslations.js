import it from './it.json'
import en from './en.json'
import de from './de.json'
export function DefaultFormioTranslations()  {
    return {
        en: en,
        de: de,
        it: it
    }
}