# Custom Form.io Core based to Bootstrap Italia

This repository implements a web component based on [formio/react](https://github.com/formio/react)

## Usage

To use Design React as a dependency in your React project you can install it from [npm](https://www.npmjs.com/~italia).

```sh
npm i @opencitylabs/formio-core
 
yarn add @opencitylabs/formio-core
```

More information on creating a new app with React:

- [Official documentation](https://react.dev/learn/start-a-new-react-project)

### Using Formio Core

```javascript
import '@opencitylabs/formio-core'
import '@opencitylabs/formio-core/dist/ocl.min.css'
```

### Form render scripts
If used in React projects
```jsx
 return (
   <render-form
        url="https://form-qa.stanzadelcittadino.it/form/6345270e7df272004dd69349"
        read-only="false"
        submission='{"data":{"sdcfile":[],"test": "testo","applicant":{"data":{"email_address":"test@test.it","completename":{"data":{"name":"Vittorino","surname":"Coliandro"}},"gender":{"data":{"gender":"maschio"}},"fiscal_code":{"data":{"fiscal_code":"CLNABD76P01G822Q"}},"Born":{"data":{"natoAIl":"01\/09\/1976","place_of_birth":"Ponsacco"}},"address":{"data":{"address":"Via Gramsci, 1","house_number":"","municipality":"Bugliano","county":"TN","postal_code":"56056"}}}}}}'>
   </render-form>
)
```
If used in Webpack projects

```html
   <render-form
        url="https://form-qa.stanzadelcittadino.it/form/6345270e7df272004dd69349"
        read-only="false"
        submission='{"data":{"sdcfile":[],"test": "testo","applicant":{"data":{"email_address":"test@test.it","completename":{"data":{"name":"Vittorino","surname":"Coliandro"}},"gender":{"data":{"gender":"maschio"}},"fiscal_code":{"data":{"fiscal_code":"CLNABD76P01G822Q"}},"Born":{"data":{"natoAIl":"01\/09\/1976","place_of_birth":"Ponsacco"}},"address":{"data":{"address":"Via Gramsci, 1","house_number":"","municipality":"Bugliano","county":"TN","postal_code":"56056"}}}}}}'>
   </render-form>
```

### Builder form scripts
If used in React projects
```jsx
 return (
   <build-form
        url="https://form-qa.stanzadelcittadino.it/form/6345270e7df272004dd69349"
        read-only="false"
        submission='{"data":{"sdcfile":[],"test": "testo","applicant":{"data":{"email_address":"test@test.it","completename":{"data":{"name":"Vittorino","surname":"Coliandro"}},"gender":{"data":{"gender":"maschio"}},"fiscal_code":{"data":{"fiscal_code":"CLNABD76P01G822Q"}},"Born":{"data":{"natoAIl":"01\/09\/1976","place_of_birth":"Ponsacco"}},"address":{"data":{"address":"Via Gramsci, 1","house_number":"","municipality":"Bugliano","county":"TN","postal_code":"56056"}}}}}}'>
   </build-form>
)
```
If used in Webpack projects

```html
   <build-form
        url="https://form-qa.stanzadelcittadino.it/form/6345270e7df272004dd69349"
        read-only="false"
        submission='{"data":{"sdcfile":[],"test": "testo","applicant":{"data":{"email_address":"test@test.it","completename":{"data":{"name":"Vittorino","surname":"Coliandro"}},"gender":{"data":{"gender":"maschio"}},"fiscal_code":{"data":{"fiscal_code":"CLNABD76P01G822Q"}},"Born":{"data":{"natoAIl":"01\/09\/1976","place_of_birth":"Ponsacco"}},"address":{"data":{"address":"Via Gramsci, 1","house_number":"","municipality":"Bugliano","county":"TN","postal_code":"56056"}}}}}}'>
   </build-form>
```

Documentation:

- [Storybook](https://opencity-labs.gitlab.io/formio-core/)
